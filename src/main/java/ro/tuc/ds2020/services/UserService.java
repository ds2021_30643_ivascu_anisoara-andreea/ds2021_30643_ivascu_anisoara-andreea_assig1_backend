package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO authentication(UserDTO userDTO)
    {
        List<User> users = userRepository.findAll();

        for(User p: users)
        {
            if((p.getUsername().equals(userDTO.getUsername())) && (p.getPassword().equals(userDTO.getPassword())))
            {
                return UserBuilder.toUserDTO(p);
            }
        }
        throw new ResourceNotFoundException("User or password not found!");
    }

}
