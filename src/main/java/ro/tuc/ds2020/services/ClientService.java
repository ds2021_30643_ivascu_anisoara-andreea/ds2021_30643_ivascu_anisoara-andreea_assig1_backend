package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.ClientDetailsDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.ClientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ClientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<ClientDTO> findClients() {
        List<Client> clientList = clientRepository.findAll();
        return clientList.stream()
                .map(ClientBuilder::toClientDTO)
                .collect(Collectors.toList());
    }

    public ClientDetailsDTO findClientById(UUID id) {
        Optional<Client> prosumerOptional = clientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Client with id {} was not found in db", id);
            throw new ResourceNotFoundException(Client.class.getSimpleName() + " with id: " + id);
        }
        return ClientBuilder.toClientDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(ClientDetailsDTO clientDTO) {
        Client client = ClientBuilder.toEntity(clientDTO);
        client = clientRepository.save(client);
        LOGGER.debug("Client with id {} was inserted in db", client.getId());
        return client.getId();
    }

    public void deleteClientById(UUID id) {
        clientRepository.deleteById(id);
        Optional<Client> toSave = clientRepository.findById(id);
        if(toSave.isPresent()){

        }

    }


    public UUID update(ClientDetailsDTO clientDTO){
        Client client = ClientBuilder.toEntity(clientDTO);
        client.setId(clientDTO.getId());
        Optional<Client> toSave = clientRepository.findById(client.getId());
        if(!toSave.isPresent()){

            throw new ResourceNotFoundException(Client.class.getSimpleName() + " with id: " + client.getId());
        }
        clientRepository.save(client);


        return client.getId();
    }





}
