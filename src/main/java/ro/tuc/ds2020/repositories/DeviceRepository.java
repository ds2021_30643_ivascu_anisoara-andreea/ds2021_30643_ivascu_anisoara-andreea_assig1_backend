package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Device;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface DeviceRepository extends JpaRepository<Device, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Device> findByDescription(String description);

    @Query(value = "SELECT d " +
            "FROM Device d " +
            "WHERE d.id = :id")
    Optional<Device> findWithId(@Param("id") UUID id);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT d " +
            "FROM Device d " +
            "WHERE d.description = :description")
    Optional<Device> findSeniorsByUsername(@Param("description") String description);


}
