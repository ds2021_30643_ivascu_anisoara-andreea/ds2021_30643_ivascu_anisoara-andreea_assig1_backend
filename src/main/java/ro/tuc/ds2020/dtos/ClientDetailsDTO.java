package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.User;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class ClientDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;

    private Date birthDate;

   // private User user;

    public ClientDetailsDTO() {
    }


    public ClientDetailsDTO(UUID id, String name, String address, Date birthDate) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthDate=birthDate;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


}
