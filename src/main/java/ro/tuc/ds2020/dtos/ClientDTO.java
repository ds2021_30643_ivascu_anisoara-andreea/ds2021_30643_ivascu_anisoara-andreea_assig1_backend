package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.User;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class ClientDTO extends RepresentationModel<ClientDTO> {
    private UUID id;
    private String name;
    private Date birthDate;
    private String address;


    public ClientDTO() {
    }



    public ClientDTO(UUID id, String name, String address, Date birthDate) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.address=address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


}
