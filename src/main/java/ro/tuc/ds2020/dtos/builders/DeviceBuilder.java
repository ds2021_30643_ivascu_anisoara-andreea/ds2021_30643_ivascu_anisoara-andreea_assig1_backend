package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.entities.Device;

public class DeviceBuilder {

    private DeviceBuilder() {
    }

    public static DeviceDTO toDeviceDTO(Device device) {
        return new DeviceDTO(
                device.getId(),
                device.getAddress(),
                device.getDescription(),
                device.getMaximumEnergyConsumption(),
                device.getAverageEnergyConsumption()

        );
    }

    public static Device toEntity(DeviceDetailsDTO deviceDetailsDTO) {
        return new Device(deviceDetailsDTO.getAddress(),
                deviceDetailsDTO.getDescription(),
                deviceDetailsDTO.getMaximumEnergyConsumption(),
                deviceDetailsDTO.getAverageEnergyConsumption()

        );
    }

    public static DeviceDetailsDTO toDeviceDetailsDTO(Device device) {
        return new  DeviceDetailsDTO (
                device.getId(),
                device.getAddress(),
                device.getDescription(),
                device.getMaximumEnergyConsumption(),
                device.getAverageEnergyConsumption()

        );
    }
}
