package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class SensorDetailsDTO {

    private UUID id;
    @NotNull
    private String description;
    @NotNull
    private float maximumValue;




    public SensorDetailsDTO() {
    }

    public SensorDetailsDTO(UUID id, String description, float maximumValue) {
        this.id = id;
        this.description = description;
        this.maximumValue = maximumValue;
    }

    public SensorDetailsDTO(String description, float maximumValue) {
        this.description = description;
        this.maximumValue = maximumValue;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(float maximumValue) {
        this.maximumValue = maximumValue;
    }
}
