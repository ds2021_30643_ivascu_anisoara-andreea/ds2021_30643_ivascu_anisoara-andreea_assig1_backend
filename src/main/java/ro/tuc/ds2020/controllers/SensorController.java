package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import ro.tuc.ds2020.dtos.ClientDetailsDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDetailsDTO;

import ro.tuc.ds2020.services.SensorService;


import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/sensor")
public class SensorController {

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping()
    public ResponseEntity<List<SensorDTO>> getSensors() {
        List<SensorDTO> dtos = sensorService.findSensors();
        for (SensorDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorController.class)
                    .getSensor(dto.getId())).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody SensorDetailsDTO sensorDTO) {
        UUID sensorID = sensorService.insert(sensorDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SensorDetailsDTO> getSensor(@PathVariable("id") UUID sensorId) {
        SensorDetailsDTO dto = sensorService.findSensorById(sensorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }



    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<HttpStatus> deleteSensor(@PathVariable("id") UUID sensorId){
        sensorService.deleteSensorById(sensorId);
        return new ResponseEntity(new SensorDTO(),HttpStatus.OK);
    }



    @PostMapping(value = "/update")
    public ResponseEntity<UUID> updateSensor(@Valid @RequestBody SensorDetailsDTO sensorDTO) {
        UUID sensorID = sensorService.update(sensorDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }




}
